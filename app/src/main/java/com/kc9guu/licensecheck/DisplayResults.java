package com.kc9guu.licensecheck;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.JsonObject;

public class DisplayResults extends AppCompatActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        Log.d( "onCreate", "Starting the display results activity" );
        setContentView( R.layout.activity_display_results );
        Log.d( "onCreate", "Setting the content view" );

        Log.d( "onCreate", "Getting the intent" );
        Intent intent = getIntent();
        Log.d( "onCreate", "Got the intent" );
        Log.d( "onCreate", "Getting the callsign" );
        String callSign = intent.getStringExtra( MainActivity.CALL_SIGN );
        Log.d( "onCreate", "Call sign is: " + callSign );
        Log.d( "onCreate", "Creating the table" );
        createTable( new JsonObject() );
    }

    @SuppressLint ( "SetTextI18n" )
    private void createTable ( JsonObject json ) {
        Log.d( "createTable", "In the createTable function" );
        TableLayout headerLayout = findViewById( R.id.resultsTableView );
        TableRow header = findViewById( R.id.headerTableRow);

        String headerName   = "Name";

        TextView headerNameText   = new TextView( this );
        TextView headerCallText   = new TextView( this );
        TextView headerActiveText = new TextView( this );

        headerNameText.setText( headerName );
        String headerCall = "Call Sign";
        headerCallText.setText( headerCall );
        String headerActive = "Active Status";
        headerActiveText.setText( headerActive );

        header.addView( headerNameText );
        header.addView( headerCallText );
        header.addView( headerActiveText );

        headerLayout.addView( header );

        TextView rowNameText   = new TextView( this );
        TextView rowCallText   = new TextView( this );
        TextView rowActiveText = new TextView( this );

        rowNameText.setText( "John Doe" );
        rowCallText.setText( "aa0aaa" );
        rowActiveText.setText( "Active" );

        TableRow row = new TableRow( this );
        row.addView( rowNameText );
        row.addView( rowCallText );
        row.addView( rowActiveText );

        headerLayout.addView( row, headerLayout.indexOfChild( header ) + 1 );
    }
}
