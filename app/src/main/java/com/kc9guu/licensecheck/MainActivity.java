package com.kc9guu.licensecheck;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String CALL_SIGN = "com.kc9guu.licensecheck.MESSAGE";

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        Log.i( "onCreate", "Starting main activity" );
        setContentView( R.layout.activity_main );
    }

    public void sendSearch ( View view ) {
        // On search, create an Intent and start the activity
        Log.i( "sendSearch", "In the sendSearch function" );
        Intent intent = new Intent( this, DisplayResults.class );
        Log.i( "sendSearch", "Got the intent" );
        EditText editText = findViewById( R.id.licenseText );
        Log.d( "sendSearch", "Got the text for the license" );
        String callSign = editText.getText().toString();
        Log.d( "sendSearch", "The callsign is: " + callSign );
        intent.putExtra( CALL_SIGN, callSign );
        Log.d( "sendSearch", "Putting the callsign in the extra field" );
        // Start the activity
        Log.d( "sendSearch", "We got a click" );
        Log.d( "sendSearch", "Starting the second activity" );
        startActivity(intent);
    }

}
