package com.kc9guu.licensecheck;

public class NonASCIIChars extends RuntimeException {
    public NonASCIIChars ( String errorMessage ) {
        super(errorMessage);
    }
}
