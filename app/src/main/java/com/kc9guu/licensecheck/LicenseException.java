package com.kc9guu.licensecheck;

class LicenseException extends RuntimeException {
    public LicenseException ( String errorMessage, Throwable err ) {
        super( errorMessage, err );
    }
}
