package com.kc9guu.licensecheck.FCC;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kc9guu.licensecheck.NonASCIIChars;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetAddress;
import java.net.URL;
import java.nio.charset.Charset;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

class License {

    JsonObject searchLicense ( String license ) throws IOException {

        if ( ! license.matches( "\\p{ASCII}*" ) ) {
            throw new NonASCIIChars( "License should only contain ASCII characters" );
        }

        String endpoint = "/api/license-view/basicSearch/getLicenses?searchValue=";
        String protocol = "https://";
        String domain = "data.fcc.gov";
        String format = "&format=json";
        InetAddress address = java.net.InetAddress.getByName( domain );
        String ipAddress = address.getHostAddress();
        String query = protocol + ipAddress + endpoint + license + format;

        return readFromURL(query);
    }

    JsonArray parseLicense ( JsonObject license ) {
        JsonObject licensesKey = (JsonObject) license.get( "Licenses" );

        return (JsonArray) licensesKey.get( "License" );
    }

    private static JsonObject readFromURL ( String fullURL ) {
        try {
            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify ( String hostname, SSLSession session ) {
                    return hostname.equals( "192.104.54.218" );
                }
            };

            try {
                URL url = new URL( fullURL );
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setRequestProperty( "Content-Type", "application/json" );
                urlConnection.setHostnameVerifier( hostnameVerifier );
                InputStream is = urlConnection.getInputStream();
                BufferedReader rd = new BufferedReader( new InputStreamReader( is, Charset.forName( "UTF-8" ) ) );
                String jsonText = readAll( rd );

                Gson g = new Gson();
                JsonParser parser = new JsonParser();
                JsonElement jsonTree = parser.parse(jsonText);
                if ( jsonTree.isJsonObject() ) {
                    return jsonTree.getAsJsonObject();
                }
            } catch (Exception e) {
                Log.e( "License", "error getting input stream" );
            }
        } catch ( Exception e ) {
            Log.e( "License", "error initializing SSL options" );
        }
        return new JsonObject();
    }

    private static String readAll( Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        //noinspection NestedAssignment
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
