package com.kc9guu.licensecheck.FCC;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class LicenseTest {

    @Test
    public void searchLicense () {
        License tester  = new License();
        JsonObject response;
        try {
            String testCall = "kc9guu";
            response = tester.searchLicense( testCall );
        } catch ( IOException e ) {
            fail( "Search license failed with an IO Exception: " + e.getMessage() );
            return;
        }

        assertThat( "Response is a JSONObject", response, instanceOf( JsonObject.class ) );
    }

    @Test
    public void parseLicense () {
        License tester = new License();
        JsonObject response;
        try {
            String testCall = "kc9guu";
            response = tester.searchLicense( testCall );
        } catch ( IOException e ) {
            fail( "Search license failed with an IO Exception: " + e.getMessage() );
            return;
        }

        JsonArray parsed;
        try {
            parsed = tester.parseLicense( response );
        } catch ( Exception e ) {
            fail( "Something happened in parsing" + e.getMessage() );
            return;
        }
        //JsonArray license = parsed.getAsJsonArray( "License" );
        HashMap<String, String> licenseHash = new HashMap<>();
        for( JsonElement item : parsed ) {
            JsonObject element = item.getAsJsonObject();
            licenseHash.put( "Name", String.valueOf( element.get( "licName" ) ) );
            licenseHash.put( "Call Sign", String.valueOf( element.get( "callsign" ) ) );
            licenseHash.put( "Status", String.valueOf( element.get( "statusDesc" ) ) );
        }

        assertThat( "After parsing, the response is a JSONObject", parsed, instanceOf( JsonArray.class) );
        String nameString = "Brandt, Joshua S";
        assertEquals( Objects.requireNonNull( licenseHash.get( "Name" ) ).replaceAll( "\"", "" ), nameString.toUpperCase() );
        String callSign = "kc9guu";
        assertEquals( Objects.requireNonNull( licenseHash.get( "Call Sign" ) ).replaceAll( "\"", "" ), callSign.toUpperCase() );
        String statusString = "Active";
        assertEquals( Objects.requireNonNull( licenseHash.get( "Status" ) ).replaceAll( "\"", "" ), statusString );
    }
}
